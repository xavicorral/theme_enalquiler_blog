<?php
/*
Template Name: 404
*/
?>
<?php get_header(); ?>
<div id="core">

	<div id="content" class="eightcol">
    
        <h2 class="leading"><?php _e('No hemos encontrado lo que buscaba!','themnific');?><br/>
        
        <span><?php _e('Tal vez encuentres algo interesante aquí ...','themnific');?></span></h2>
    
    	<div class="entry">
                
                <?php get_template_part('/includes/uni-404-content');?>
    
        </div>
        
	</div>

    <?php get_sidebar(); ?>  

</div><!-- #core -->

<div class="clearfix"></div>
<?php get_footer(); ?>