<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
<div id="core">

	<div id="content" class="eightcol">

        <div <?php post_class(); ?>>
    
            <div class="entry" itemprop="text">
    
                <h1 class="post entry-title" itemprop="headline"><?php the_title(); ?></h1>
            
                <?php the_content(); ?>
            
                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:','themnific') . '</span>', 'after' => '</div>' ) ); ?>
                
                <?php the_tags( '<p class="tagssingle">','',  '</p>'); ?>
                
            </div>                 
          
            <?php comments_template(); ?>
            
        </div>

    </div>

	<?php endwhile; else: ?>

		<p><?php _e('Sorry, no posts matched your criteria','themnific');?>.</p>

	<?php endif; ?>

	<?php get_sidebar(); ?> 
    
</div>

<?php get_footer(); ?>