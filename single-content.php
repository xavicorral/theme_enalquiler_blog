<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="entry" itemprop="text">
        
	<?php 
        $entradilla = get_post_meta($post->ID, 'tmnf_entradilla', true);
        $video_input = get_post_meta($post->ID, 'tmnf_video', true);
        $audio_input = get_post_meta($post->ID, 'tmnf_audio', true);
        $rating = get_post_meta($post->ID, 'tmnf_rating_main', true);
		$sidebar_opt = get_post_meta($post->ID, 'tmnf_sidebar', true);
		$single_featured = get_post_meta($post->ID, 'tmnf_single_featured', true);
    ?>
    
    <?php 	
        if(has_post_format('video')){
                echo ($video_input);
        }elseif(has_post_format('audio')){
                echo ($audio_input);
                 echo ($entradilla);
		}elseif(has_post_format('gallery')){

			if ($single_featured == 'Yes')  {
			   the_post_thumbnail('format-single');  
			} else;	
        } else {
            if (get_option('themnific_post_image_dis') == 'true' );
            else
               the_post_thumbnail('format-standard',array('itemprop' => 'image'));  
    }?>

    <div class="clearfix"></div>
    
    <?php tmnf_meta() ?>
    <div class="bloqueentradilla">
                              <?php echo '<span class="entradilla">'.$entradilla.'</span>' ; ?>

    </div>
    <?php if($rating) { get_template_part( '/includes/mag-rating' );  } else { }?>  

    <?php the_content(); ?>
    
    <?php 
	
		wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:','themnific') . '</span>', 'after' => '</div>' ) );
    	the_tags( '<p>' . __( 'Tags: ','themnific') . '', ', ', '</p>');
		
    ?>
    
    <div class="clearfix"></div>
    
</div><!-- end .entry -->

<?php if($sidebar_opt == 'None'){  get_template_part('single-sidebar' );} else {} ?>
    
    <div class="clearfix"></div>
    
    <?php get_template_part('/includes/mag-postad'); ?>
    
    <?php comments_template(); ?>

<?php endwhile; else: ?>

	<p><?php _e('Sorry, no posts matched your criteria','themnific');?>.</p>

<?php endif; ?>