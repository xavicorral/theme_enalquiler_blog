		<div class="cleafix"></div>
        <div class="postauthor vcard author" itemprop="author" itemscope itemtype="http://schema.org/Person">
			<?php $photoauthor = get_the_author_meta('photo');
			echo '<img class="postauthor" src="'.$photoauthor.'" />'; ?>
        	<h4><?php _e('Sobre el autor','themnific');?></h4> <h3  class="additional" itemprop="name"><?php the_author_posts_link(); ?></h3>
 			<div class="authordesc"><?php the_author_meta('description'); ?></div>
 			<?php the_author_meta('url'); ?>
 			                    <br /><a href="<?php echo the_author_meta('googleplus') ?>">Google+ </a> 

		</div>
		<div class="cleafix"></div>