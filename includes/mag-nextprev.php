		<span>
            <i class="icon-chevron-sign-left"></i> <?php _e('Anterior','themnific');?>:<br/>
            <?php previous_post_link('%link'); ?>
        </span>
        
        <span>
            <?php _e('Siguiente','themnific');?>: <i class="icon-chevron-sign-right"></i><br/>
            <?php next_post_link('%link'); ?>
        </span>