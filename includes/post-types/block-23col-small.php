				<li class="fblock fblock_small">
            
					<?php if ( has_post_thumbnail()) : ?>
                    
                         <a href="<?php the_permalink(); ?>" title="<?php echo short_title('...', 6); ?>" >
                         
                         	<?php the_post_thumbnail( 'blog-small'); ?>
                            
                         </a>
                            
                        <h4><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 11); ?></a></h4>
                        
                        <?php tmnf_meta_small() ?>
                         
                    <?php endif; ?>

				</li><!-- end small twin -->