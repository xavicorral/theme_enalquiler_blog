</div><div class="clerfix"></div>


	<footer id="footer-ena">

            <div class="footer-social-links">
                <div class="container">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <a target="_blank" href="http://www.facebook.com/Enalquiler" title="http://www.facebook.com/Enalquiler" rel="nofollow">
                                <i class="fa fa-facebook-square left"></i>
                                <p>Facebook</p>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/115975913314772877389?prsrc=3" rel="nofollow">
                                <i class="fa fa-google-plus left"></i>
                                <p>Google+</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://twitter.com/enalquilercom" rel="nofollow">
                                <i class="fa fa-twitter left"></i>
                                <p>Twitter</p>
                            </a>
                        </li>
                        <li>
                            <a class="pinterest" href="http://www.pinterest.com/enalquiler/" rel="nofollow">
                                <i class="fa fa-pinterest left"></i>
                                <p class="hidden-xs">Pinterest</p>
                            </a>
                        </li>
                        <li>
                            <a class="instagram" href="http://instagram.com/enalquiler" rel="nofollow">
                                <i class="fa fa-instagram left"></i>
                                <p class="hidden-xs">Instagram</p>
                            </a>
                        </li>
                        <!--
                        <li class="last">
                            <a href="http://blog.enalquiler.com/" rel="nofollow">
                                <i class="fa fa-rss left"></i>
                                <p>Blog</p>
                            </a>
                        </li>
                        -->
                    </ul>
                </div>
            </div>


            <div class="footer-intra-links">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-4 info-mercado-alquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Información sobre el <b>Mercado del Alquiler</b></div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.enalquiler.com/precios/precio-alquiler-vivienda-espana_31-0-0-0.html" rel="nofollow">Evolución del precio del alquiler</a></li>
                                <li><a href="http://www.enalquiler.com/ventajas-de-alquilar-para-el-propietario.html" rel="nofollow">Ventajas de alquilar: para el propietario</a></li>
                                <li><a href="http://www.enalquiler.com/ventajas-de-alquilar-para-el-inquilino.html" rel="nofollow">Ventajas de alquilar: para el inquilino</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 secciones-interes">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i><b>Enalquiler</b> en la red</div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.enalquiler.com/index.php/cod.go_page/page.frm_recomienda/" rel="nofollow">¡Recomienda Enalquiler a un amigo!</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 sobre-enalquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Sobre <b>Enalquiler</b></div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.enalquiler.com/que-es-enalquiler-com.html" rel="nofollow">¿Qué es enalquiler.com?</a></li>
                                <li><a href="http://www.enalquiler.com/index.php/cod.faq_ayuda/" rel="nofollow">Preguntas frecuentes - Ayuda</a></li>
                                <li><a href="http://www.enalquiler.com/publicidad.html" rel="nofollow">Publicidad</a></li>
                                <li><a href="http://www.enalquiler.com/aviso-legal.html" rel="nofollow">Aviso Legal</a></li>
                                <li><a href="http://www.enalquiler.com/sitemap/" rel="nofollow">Sitemap</a></li>
                                <li><a href="http://www.enalquiler.com/busquedas/0/1" rel="nofollow"Últimas búsquedas</a></li>
                                <li><a href="http://www.enalquiler.com/anunciar-piso-gratis" rel="nofollow"<b>Anuncia tu piso gratis</b></a></li>
                                <li><a href="http://www.enalquiler.com/servicios-anunciantes.html" rel="nofollow"Servicios para anunciantes profesionales</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>


            <div class="grupo-intercom">
                <div class="container">
                    <img src="http://www.enalquiler.com/front-end/images/grupo-intercom.png" alt="Grupo Intercom">
                </div>
            </div>

        </footer> 
        
	</div><!--  end .cotainer  --> 
   

<div class="scrollTo_top" style="display: block">

    <a title="Scroll to top" href="#">
    
    	<i class="fa fa-arrow-circle-o-up"></i>
        
    </a>
    
</div>
<?php themnific_foot(); ?>
<?php wp_footer();?>

</body>
</html>