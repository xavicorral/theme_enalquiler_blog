<?php 

/*-----------------------------------------------------------------------------------*/
/* Themnific Framework  & Theme Version */
/*-----------------------------------------------------------------------------------*/


function themnific_version(){
    
    $theme_data = wp_get_theme();
	
}
add_action('wp_head','themnific_version');

/*-----------------------------------------------------------------------------------*/
/* Load the required Framework Files */
/*-----------------------------------------------------------------------------------*/

$functions_path = get_template_directory() . '/functions/';

get_template_part('/functions/admin-setup');			// Options panel variables and functions
get_template_part('/functions/admin-functions');		// Custom functions and plugins
get_template_part('/functions/admin-interface');		// Admin Interfaces
get_template_part('/functions/admin-hooks');			// Definition of Themnific Hooks

/*-----------------------------------------------------------------------------------*/
/* AQUA */
/*-----------------------------------------------------------------------------------*/

//definitions
if(!defined('AQPB_VERSION')) define( 'AQPB_VERSION', '1.1.2' );
if(!defined('AQPB_PATH')) define( 'AQPB_PATH', get_template_directory() . '/functions/aqua/' );
if(!defined('AQPB_DIR')) define( 'AQPB_DIR', get_template_directory() . '/functions/aqua/' );

$aqua_path = get_template_directory() . '/functions/aqua/';

//required functions & classes
require_once ($aqua_path . 'functions/aqpb_config.php');
require_once ($aqua_path . 'functions/aqpb_blocks.php');
require_once ($aqua_path . 'classes/class-aq-page-builder.php');
require_once ($aqua_path . 'classes/class-aq-block.php');
require_once ($aqua_path . 'functions/aqpb_functions.php');

// default blocks
require_once(AQPB_PATH . 'blocks/aq-text-block.php');
require_once(AQPB_PATH . 'blocks/aq-clear-block.php');
require_once(AQPB_PATH . 'blocks/aq-widgets-block.php');
// custom blocks
require_once(AQPB_PATH . 'blocks/aq-3-column-block.php');
//require_once(AQPB_PATH . 'blocks/aq-2-column-block.php');
require_once(AQPB_PATH . 'blocks/aq-2-3-column-block.php');
require_once(AQPB_PATH . 'blocks/aq-featured-3col-block.php');
require_once(AQPB_PATH . 'blocks/aq-featured-2-3col-small-block.php');
require_once(AQPB_PATH . 'blocks/aq-featured-2-3col-big-block.php');
require_once(AQPB_PATH . 'blocks/aq-featured-2-3col-block.php');
require_once(AQPB_PATH . 'blocks/aq-flexslider-block.php');
require_once(AQPB_PATH . 'blocks/aq-flexcarousel-block.php');
require_once(AQPB_PATH . 'blocks/aq-masonry-block.php');
require_once(AQPB_PATH . 'blocks/aq-ads-block.php');

//register default blocks
aq_register_block('AQ_Text_Block');
aq_register_block('AQ_Clear_Block');
aq_register_block('AQ_Widgets_Block');
// register custom blocks
aq_register_block('AQ_3_Column_Block');
//aq_register_block('AQ_2_Column_Block');
aq_register_block('AQ_2_3_Column_Block');
aq_register_block('AQ_Featured_2_3_Block');
aq_register_block('AQ_Featured_2_3_Small_Block');
aq_register_block('AQ_Featured_2_3_Big_Block');
aq_register_block('AQ_Featured_3_Block');
aq_register_block('AQ_Flexslider_Block');
aq_register_block('AQ_Flexcarousel_Block');
aq_register_block('AQ_Masonry_Block');
aq_register_block('AQ_Ads_Block');

//fire up page builder
$aqpb_config = aq_page_builder_config();
$aq_page_builder = new AQ_Page_Builder($aqpb_config);
if(!is_network_admin()) $aq_page_builder->init();

?>