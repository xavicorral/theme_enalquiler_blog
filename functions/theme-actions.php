<?php 
/*-----------------------------------------------------------------------------------*/
/* Custom functions */
/*-----------------------------------------------------------------------------------*/


	global $themnific_options;
	$output = '';

// Add custom styling
add_action('wp_head','themnific_custom_styling');
function themnific_custom_styling() {
	
	// Get options
	$home = home_url();
	$home_theme  = get_template_directory_uri();
	
	$sec_body_color = get_option('themnific_custom_color');
	$thi_body_color = get_option('themnific_thi_body_color');
	$for_body_color = get_option('themnific_for_body_color');
	$body_color = get_option('themnific_body_color');
	$background_color = get_option('themnific_background_color');
	$text_color = get_option('themnific_text_color');
	$text_color_alter = get_option('themnific_text_color_alter');
	$body_color_sec = get_option('themnific_body_color_sec');
	$sec_text_color = get_option('themnific_sec_text_color');
	$thi_text_color = get_option('themnific_thi_text_color');
	$link = get_option('themnific_link_color');
	$link_alter = get_option('themnific_thi_link_color');
	$hover = get_option('themnific_link_hover_color');
	$sec_link = get_option('themnific_sec_link_color');
	$sec_hover = get_option('themnific_sec_link_hover_color');
	$thi_hover = get_option('themnific_thi_link_hover_color');
	$body_bg = get_option('themnific_body_bg');
	$body_bg_sec = get_option('themnific_body_bg_sec');
	$shadows = get_option('themnific_shadows_color');
	$shadows_sec = get_option('themnific_shadows_color_sec');
	$shadows_thi = get_option('themnific_shadows_color_thi');
	$border = get_option('themnific_border_color');
	$border_sec = get_option('themnific_border_color_sec');

	    $custom_css = get_option('themnific_custom_css');
		
	// Add CSS to output
	
if ($custom_css)
	$output .= $custom_css ;
	$output = '';

if ($body_color)
	$output .= '
	#header,#navigation,.nav li ul,ul.social-menu .social-menu-more li,.flexcarousel ul.slides li,#hometab,.postinfo,.widgetable,ul.products li.product {background-color:'.$body_color.'}' . "\n";
	$output .= '
	.nav>li>ul:after{border-color:'.$body_color.' transparent}' . "\n";
	$output .= '
	#main-nav>li>a,.searchform,ul.social-menu li{border-color:'.$body_color.' !important}' . "\n";
	$output .= '
	.woocommerce-message,.woocommerce-error,.woocommerce-info{background:'.$body_color.' !important}' . "\n";
	
if ($sec_body_color)
	$output .= '
	#footer,.body2,h2.widget_spec,.imgwrap,#topnav,.maso,.mainflex .videoinside{background-color:'.$sec_body_color.'}' . "\n";
	
if ($thi_body_color)
	$output .= '
	.body3{background-color:'.$thi_body_color.'}' . "\n";
	
if ($for_body_color)
	$output .= '
	.socialtrigger a:hover,.socialtrigger a.active,.overrating,.meta_more a:hover,.sf-sub-indicator,span.score,.flexslider a.flex-prev,.flexslider a.flex-next,.ratingbar,#sidebar p input[type=submit],span.ribbon,a#triggernav,a#triggernav-sec,a.fromhome,a.mainbutton,a.itembutton,.page-numbers.current,a.comment-reply-link,#submit,#comments .navigation a,.tagssingle a,.contact-form .submit,.intro,li.main h2,.plan-bottom a,.scrollTo_top a,.gallery-item, submit{background-color:'.$for_body_color.'}' . "\n";
	$output .= 'h3 a i,h4 a i,.meta_more a,a.moreposts,.more a.fr,.entry a,li ul li.current-menu-item a,.woocommerce a.button{color:'.$for_body_color.' !important}' . "\n";
	$output .= 'h2.widget,h2.widget_spec,.meta_more a,#serinfo-nav li.current a,.woocommerce a.button{border-color:'.$for_body_color.' !important}' . "\n";
	$output .= '#main-nav>li>a:hover,#main-nav>li.current-menu-ancestor>a,#main-nav>li.current-menu-item>a,#main-nav>li.sfHover>a,#main-nav>li.current_page_item>a,#main-nav>li.current-menu-item>a,.page-numbers.current,.woocommerce a.button:hover{background-color:'.$for_body_color.' !important}' . "\n";
	

	
if ($background_color)
	$output .= '
	body,#navigation,.social-menu-more{background-color:'.$background_color.'}' . "\n";

if ($text_color)
	$output .= '
	body,.body1,.tabbig p {color:'.$text_color.'}' . "\n";
		
if ($sec_text_color)
	$output .= '
	.body2{color:'.$sec_text_color.'}' . "\n";
	
if ($text_color_alter)
	$output .= '.body3XXX {color:'.$text_color_alter.' !important}' . "\n";
	
if ($link)
	$output .= '
	.body1 a, a:link, a:visited,.nav li ul li a{color:'.$link.'}' . "\n";
	$output .= '
	span>a.moreposts,ul#serinfo-nav li a{color:'.$link.' !important}' . "\n";
	
if ($link_alter)
	$output .= '
	.deff{color:'.$link_alter.'}' . "\n";
	
if ($hover)
	$output .= '
	a:hover,.body1 a:hover{color:'.$hover.'  !important}' . "\n";
	
if ($sec_link)
	$output .= '
	#sec-nav > li > a,#footer a {color:'.$sec_link.'}' . "\n";
if ($sec_hover)
	$output .= '
	.body2 a:hover,p.body2 a:hover,#sec-nav > li > a:hover{color:'.$sec_hover.' !important}' . "\n";
	
if ($thi_hover)
	$output .= '
	.deff{color:'.$thi_hover.' !important}' . "\n";
	


if ($body_bg)
	$output .= '
	body,#top-nav{background-image:url('.$home_theme.'/images/bg/'.$body_bg.')}' . "\n";
	
	
if ($border)
	$output .= '
	.woocommerce ul.products,#navigation,#header,.aq-block,.fblock,ul#serinfo,.postinfo,.postinfo span,.widgetable,.related,#hometab,h2.leading,.postauthor_alt{border-color:'.$border.' !important}' . "\n";	
	
if ($border_sec)
	$output .= '
	.deff{border-color:'.$border_sec.' !important}' . "\n";

		




		// General Typography		
		$font_text = get_option('themnific_font_text');	
		$font_text_sec = get_option('themnific_font_text_sec');	
		$font_text_thi = get_option('themnific_font_text_thi');	
		
		$font_nav = get_option('themnific_font_nav');
		$font_h1 = get_option('themnific_font_h1');	
		$font_h2 = get_option('themnific_font_h2');	
		$font_h2_widget = get_option('themnific_font_h2_widget');	
		$font_h3 = get_option('themnific_font_h3');	
		$font_h4 = get_option('themnific_font_h4');	
		$font_h5 = get_option('themnific_font_h5');	
		$font_h6 = get_option('themnific_font_h5');	
		
		
		$font_h2_tagline = get_option('themnific_font_h2_tagline');	
	
	
if ( $font_text )
	$output .= '
	body,input, textarea,input checkbox,input radio,select, file,h3.sd-title {font:'.$font_text["style"].' '.$font_text["size"].'px/1.8em '.stripslashes($font_text["face"]).';color:'.$font_text["color"].'}' . "\n";
	
if ( $font_text_sec )
	$output .= '
	.body2,#footer,.searchformhead>input.s {font:'.$font_text_sec["style"].' '.$font_text_sec["size"].'px/1.8em '.stripslashes($font_text_sec["face"]).';color:'.$font_text_sec["color"].'}' . "\n";
	$output .= '
	.ratingblock h2,.ratingblock h3,.ratingblock p,#footer h2,.body2 h2,.body2 h3{color:'.$font_text_sec["color"].'}';	
	
if ( $font_text_thi )
	$output .= '
	.intro{font:'.$font_text_thi["style"].' '.$font_text_thi["size"].'px/2.2em '.stripslashes($font_text_thi["face"]).';color:'.$font_text_thi["color"].'}' . "\n";
	$output .= '
	.intro h1,.intro h1 a,a.itembutton,a.mainbutton,.page-numbers.current{color:'.$font_text_thi["color"].'}' . "\n";

if ( $font_h1 )
	$output .= '
	h1,h2.post {font:'.$font_h1["style"].' '.$font_h1["size"].'px/1.0em '.stripslashes($font_h1["face"]).';color:'.$font_h1["color"].'}';
		
if ( $font_h2_widget )
	$output .= '
	h2.widget,.meta_more a,a.morebutton,ul#serinfo-nav li a {font:'.$font_h2_widget["style"].' '.$font_h2_widget["size"].'px/1.2em '.stripslashes($font_h2_widget["face"]).';color:'.$font_h2_widget["color"].'}';
	
if ( $font_h2 )
	$output .= '
	h2 {font:'.$font_h2["style"].' '.$font_h2["size"].'px/1.2em '.stripslashes($font_h2["face"]).';color:'.$font_h2["color"].'}';
	
if ( $font_h3 )
	$output .= '
	h3,h3#reply-title,#respond h3,.comment-author cite{font:'.$font_h3["style"].' '.$font_h3["size"].'px/1.0em '.stripslashes($font_h3["face"]).';color:'.$font_h3["color"].'}';
	
if ( $font_h4 )
	$output .= '
	h4,li.product h3,.woocommerce .upsells.products h2,.woocommerce .related.products h2,.woocommerce ul.cart_list li a,.woocommerce ul.product_list_widget li a,.woocommerce-page ul.cart_list li a,.woocommerce-page ul.product_list_widget li a {font:'.$font_h4["style"].' '.$font_h4["size"].'px/1.5em '.stripslashes($font_h4["face"]).';color:'.$font_h4["color"].'}';	
	
if ( $font_h5 )
	$output .= '
	h5,p.meta,.meta a{font:'.$font_h5["style"].' '.$font_h5["size"].'px/1.5em '.stripslashes($font_h5["face"]).';color:'.$font_h5["color"].'}';	
	
if ( $font_h6 )
	$output .= '
	h6 {font:'.$font_h6["style"].' '.$font_h6["size"].'px/1.5em '.stripslashes($font_h6["face"]).';color:'.$font_h6["color"].'}' . "\n";
	
	
if ( $font_nav )
	$output .= '
	#main-nav>li>a,.searchform input.s,.woocommerce a.button{font:'.$font_nav["style"].' '.$font_nav["size"].'px/1.0em '.stripslashes($font_nav["face"]).';color:'.$font_nav["color"].'}';
	$output .= '
	ul.social-menu li a{font:'.$font_nav["style"].' '.$font_nav["size"].'px/1.0em }';	


// custom stuff	
if ( $font_text )
	$output .= '.tab-post small a,.taggs a {color:'.$font_text["color"].'}' . "\n";	
	
	// Output styles
		if ($output <> '') {
			$output = "<!-- Themnific Styling -->\n<style type=\"text/css\">\n" . $output . "</style>\n";
			echo $output;
	}
		
} 


// google fonts link generator
function themnific_additional_styling() {
	
	get_template_part('/functions/admin-fonts');
	
} 
add_action('themnific_head','themnific_additional_styling'); 
?>