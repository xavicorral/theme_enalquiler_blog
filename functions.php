<?php

/*-----------------------------------------------------------------------------------
- Default
----------------------------------------------------------------------------------- */

add_action( 'after_setup_theme', 'tmnf_theme_setup' );

function tmnf_theme_setup() {
	global $content_width;

	/* Set the $content_width for things such as video embeds. */
	if ( !isset( $content_width ) )
		$content_width = 960;

	/* Add theme support for automatic feed links. */
	add_editor_style();
	add_theme_support( 'post-formats', array( 'video','audio', 'gallery', 'image', 'quote', 'link' ) );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'woocommerce' );

	/* Add theme support for post thumbnails (featured images). */
	if (function_exists('add_theme_support')) {
		add_theme_support('post-thumbnails');
		add_image_size('blog-3', 320, 210 ); //(cropped)
		add_image_size('blog-big', 362, 320 ); //(cropped)
		add_image_size('blog-small', 90, 90, true ); //(cropped)
		add_image_size('flex-slider', 660, 490, true ); //(cropped)
		add_image_size('flex-carousel', 204, 173, true ); //(cropped)
			add_image_size('maso1', 700, 405, true ); //(cropped)
			add_image_size('maso2', 335, 280, true ); //(cropped)
			add_image_size('maso3', 340, 350, true ); //(cropped)
		add_image_size('gallery-slider', 1040, 750, true ); //(cropped)
		add_image_size('related', 186, 140, true ); //(cropped)
	}
	
	function thumb_url(){
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 819,400 ));
	return $src[0];
	}

	/* Add your nav menus function to the 'init' action hook. */
	add_action( 'init', 'tmnf_register_menus' );

	/* Add your sidebars function to the 'widgets_init' action hook. */
	add_action( 'widgets_init', 'tmnf_register_sidebars' );
}

function tmnf_register_menus() {
	register_nav_menus(array(
			'main-menu' => "Main Menu",
			'secondary-menu' => "Secondary Menu (Top menu)"
	));
}

function tmnf_register_sidebars() {
	
	register_sidebar(array('name' => 'Sidebar','description' => 'Sidebar widget section (displayed on posts, pages and archives)','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	
	//footer widgets
	register_sidebar(array('name' => 'Footer 1','description' => 'Widget section in footer - left','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Footer 2','description' => 'Widget section in footer - center/left','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Footer 3','description' => 'Widget section in footer - center/right','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Footer 4','description' => 'Widget section in footer - right','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	
	
	register_sidebar(array('name' => 'Free1','description' => 'Free widget section (can be selected in Page Builder)','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Free2','description' => 'Free widget section (can be selected in Page Builder)','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Free3','description' => 'Free widget section (can be selected in Page Builder)','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	register_sidebar(array('name' => 'Free4','description' => 'Free widget section (can be selected in Page Builder)','before_widget' => '','after_widget' => '','before_title' => '<h2 class="widget">','after_title' => '</h2>'));
	
}

	
/*-----------------------------------------------------------------------------------
- Start Vergo Framework - Please refrain from editing this section 
----------------------------------------------------------------------------------- */

// Set path to Vergo Framework and theme specific functions
$functions_path = get_template_directory() . '/functions/';
$includes_path = get_template_directory() . '/functions/';

// Framework
require_once ($functions_path . 'admin-init.php');						// Framework Init

// Theme specific functionality
require_once ($includes_path . 'theme-options.php'); 					// Options panel settings and custom settings
require_once ($includes_path . 'theme-actions.php');					// Theme actions & user defined hooks

if (get_option('themnific_slider_gallery') <> "false") {require_once ($includes_path . 'theme-gallery.php'); } // Replace [gallery] shortcode with slider

//Add Custom Post Types
require_once ($includes_path . 'posttypes/post-metabox.php'); 			// custom meta box



// Shordcodes
require_once (get_template_directory().'/functions/admin-shortcodes.php' );				// Shortcodes
require_once (get_template_directory().'/functions/admin-shortcode-generator.php' ); 	// Shortcode generator 


	
/*-----------------------------------------------------------------------------------
- Enqueues scripts and styles for front end
----------------------------------------------------------------------------------- */ 

function tmnf_enqueue_style() {
	
	// Main stylesheet
	wp_enqueue_style( 'default_style', get_stylesheet_uri());
	
	// Layout stylesheet
	wp_enqueue_style('layout', get_stylesheet_directory_uri().	'/style-layout.css');
	
	// prettyPhoto css
	wp_enqueue_style('prettyPhoto', get_template_directory_uri() .	'/styles/prettyPhoto.css');
	
	// Shortcodes stylesheet
	wp_enqueue_style( 'shortcodes', get_template_directory_uri() . '/functions/css/shortcodes.css' );
	
	// Font Social Media css			
	wp_enqueue_style('social-media', get_template_directory_uri() .	'/styles/social-media.css');
	
	// Font Awesome css	
	wp_enqueue_style('font-awesome.min', get_template_directory_uri() .	'/styles/font-awesome.min.css');
	
	// Custom stylesheet
	wp_enqueue_style('style-custom', get_stylesheet_directory_uri() .	'/style-custom.css');
	
	
}
add_action( 'wp_enqueue_scripts', 'tmnf_enqueue_style' );




// themnific custom css + chnage the order of how the sytlesheets are loaded, and overrides WooCommerce styles.
function tmnf_custom_order() {
	
	// place wooCommerce styles before our main stlesheet
	if ( class_exists('woocommerce') ) {
		wp_dequeue_style( 'woocommerce_frontend_styles' );
		wp_enqueue_style('woocommerce_frontend_styles', plugins_url() .'/woocommerce/assets/css/woocommerce.css');
	}
	
	wp_enqueue_style('woo-custom', get_stylesheet_directory_uri().	'/styles/woo-custom.css');
	wp_enqueue_style('mobile', get_stylesheet_directory_uri().'/style-mobile.css');
}
add_action('wp_enqueue_scripts', 'tmnf_custom_order');


function tmnf_enqueue_script() {

	// Load general scripts	
		wp_enqueue_script('jquery');
		wp_enqueue_script('css3-mediaqueries', get_template_directory_uri().'/js/css3-mediaqueries.js');
		wp_enqueue_script('superfish', get_template_directory_uri().'/js/superfish.js','','', true);
		wp_enqueue_script('jquery.hoverIntent.minified', get_template_directory_uri().'/js/jquery.hoverIntent.minified.js','','', true);
		wp_enqueue_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js','','', true);
		wp_enqueue_script('ownScript', get_template_directory_uri() .'/js/ownScript.js','','', true);
		
		// Load Mobile script		
		if (get_option('themnific_res_mode_general') <> "true") {	
			wp_enqueue_script('mobile', get_template_directory_uri() .'/js/mobile.js','','', true);
		}
		
		// Load homepage slider scripts		
		if (is_home()||is_front_page()||is_page_template('index.php')||is_page_template('homepage.php')) {
			wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() .'/js/jquery.flexslider-min.js','','', true);
		}
		
		// Load folio item slider scripts		
		if ( is_home()||is_front_page()||is_single()|| is_page_template('template-fullwidth.php') ) {
			wp_enqueue_script('jquery.flexslider-min', get_template_directory_uri() .'/js/jquery.flexslider-min.js','','', true);
			wp_enqueue_script('jquery.flexslider.start.single', get_template_directory_uri() .'/js/jquery.flexslider.start.single.js','','', true);
		}
		
		// Singular comment script		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

}
	
add_action('wp_enqueue_scripts', 'tmnf_enqueue_script');

/*-----------------------------------------------------------------------------------
- Loads all the .php files found in /admin/widgets/ directory
----------------------------------------------------------------------------------- */

	$preview_template = _preview_theme_template_filter();

	if(!empty($preview_template)){
		$widgets_dir = WP_CONTENT_DIR . "/themes/".$preview_template."/functions/widgets/";
	} else {
    	$widgets_dir = WP_CONTENT_DIR . "/themes/".get_option('template')."/functions/widgets/";
    }
    
    if (@is_dir($widgets_dir)) {
		$widgets_dh = opendir($widgets_dir);
		while (($widgets_file = readdir($widgets_dh)) !== false) {
  	
			if(strpos($widgets_file,'.php') && $widgets_file != "widget-blank.php") {
				include_once($widgets_dir . $widgets_file);
			
			}
		}
		closedir($widgets_dh);
	}

	
/*-----------------------------------------------------------------------------------
- Other theme functions
----------------------------------------------------------------------------------- */

// Use shortcodes in text widgets.
add_filter('widget_text', 'do_shortcode');

// Make theme available for translation
load_theme_textdomain( 'themnific', get_template_directory() . '/lang' );

// Shorten post title
function short_title($after = '', $length) {
	$mytitle = explode(' ', get_the_title(), $length);
	if (count($mytitle)>=$length) {
		array_pop($mytitle);
		$mytitle = implode(" ",$mytitle). $after;
	} else {
		$mytitle = implode(" ",$mytitle);
	}
	return $mytitle;
}

// icons - font awesome
function tmnf_icon() {
	
	if(has_post_format('audio')) {return '<i class="fa fa-music"></i>';
	}elseif(has_post_format('gallery')) {return '<i class="fa fa-picture-o"></i>';	
	}elseif(has_post_format('link')) {return '<i class="fa fa-sign-out"></i>';			
	}elseif(has_post_format('quote')) {return '<i class="fa fa-quote-right"></i>';	
	} else {'';}	
	
}


// icons ribbons - font awesome
function tmnf_ribbon() {
	if(has_post_format('video')) {return '<span class="ribbon video-ribbon"></span><span class="ribbon_icon"><i class="fa fa-play-circle"></i></span>';
	}elseif(has_post_format('audio')) {return '<span class="ribbon audio-ribbon"></span><span class="ribbon_icon"><i class="fa fa-microphone"></i></span>';
	}elseif(has_post_format('gallery')) {return '<span class="ribbon gallery-ribbon"></span><span class="ribbon_icon"><i class="fa fa-picture-o"></i></span>';
	}elseif(has_post_format('link')) {return '<span class="ribbon link-ribbon"></span><span class="ribbon_icon"><i class="fa fa-link"></i></span>';
	}elseif(has_post_format('image')) {return '<span class="ribbon image-ribbon"></span><span class="ribbon_icon"><i class="fa fa-camera"></i></span>';
	}elseif(has_post_format('quote')) {return '<span class="ribbon quote-ribbon"></span><span class="ribbon_icon"><i class="fa fa-quote-right"></i></span>';
	} else {}	
	
}



// link format
function tmnf_link() {
	$linkformat = get_post_meta(get_the_ID(), 'tmnf_linkss', true);
	if($linkformat) echo $linkformat; else the_permalink();
}

// ratings

function tmnf_rating() {
	$rinter = get_post_meta(get_the_ID(), 'tmnf_rating_main', true);
	if ($rinter > 0) {
	return  $rinter .'<span>&#37;</span>';}
}

function tmnf_rating_plus() {
	$rinter = get_post_meta(get_the_ID(), 'tmnf_rating_main', true);
	if ($rinter > 0) {
	return  $rinter .'<span>&#37;</span>';}
}

function tmnf_ratingbar() {
	$rinter = get_post_meta(get_the_ID(), 'tmnf_rating_main', true);
	if ($rinter > 0) {
	return  '<span class="ratingbar">
				<span class="overrating" style="width:'.$rinter .'%"></span>
				<span class="overratingnr">'. $rinter .'<br/><span>&#37;</span></span>
			</span>';}
}

function tmnf_ratings() {
	$rinter = get_post_meta(get_the_ID(), 'tmnf_rating_main', true);
	if ($rinter >= 94 && $rinter <= 100) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
													</span>';}
													
	if ($rinter >= 85 && $rinter < 94) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</span>';}
													
	if ($rinter >= 75 && $rinter < 84) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-o"></i>
													</span>';}
													
	if ($rinter >= 65 && $rinter < 74) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}
													
	if ($rinter >= 55 && $rinter < 64) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}
													
	if ($rinter >= 45 && $rinter < 54) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}

	if ($rinter >= 35 && $rinter < 44) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}

	if ($rinter >= 25 && $rinter < 34) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}

	if ($rinter >= 15 && $rinter < 24) {return '	<span class="rating_star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}

	if ($rinter > 0 && $rinter < 14) {return '	<span class="rating_star">
														<i class="fa fa-star-half-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
														<i class="fa fa-star-o"></i>
													</span>';}

	if ($rinter = 0 ) {return '';}											
}


function tmnf_rating_cat() {
	$rcat = get_post_meta(get_the_ID(), 'tmnf_rating_category', true);
	$rcatcustom = get_post_meta(get_the_ID(), 'tmnf_rating_category_own', true);
	
		if($rcatcustom){
			return '<span class="nr customcat">'. $rcatcustom .'</span>';
		}elseif($rcat){
			return '<span class="nr '. $rcat .'">'. $rcat .'</span>';
		} else { }  
			
}


// new excerpt function

function tmnf_excerpt($length_callback='', $more_callback='') {
    global $post;
    if(function_exists($length_callback)){
    add_filter('excerpt_length', $length_callback);
    }
    if(function_exists($more_callback)){
    add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>'.$output.'</p>';
    echo $output;
    }



// Old Shorten Excerpt text for use in theme
function themnific_excerpt($text, $chars = 1620) {
	$text = $text." ";
	$text = substr($text,0,$chars);
	$text = substr($text,0,strrpos($text,' '));
	$text = $text."...";
	return $text;
}

function trim_excerpt($text) {
     $text = str_replace('[', '', $text);
     $text = str_replace(']', '', $text);
     return $text;
    }
add_filter('get_the_excerpt', 'trim_excerpt');





// automatically add prettyPhoto rel attributes to embedded images
function gallery_prettyPhoto ($content) {
	return str_replace("<a", "<a rel='prettyPhoto[gallery]'", $content);
}

function insert_prettyPhoto_rel($content) {
	$pattern = '/<a(.*?)href="(.*?).(bmp|gif|jpeg|jpg|png)"(.*?)>/i';
  	$replacement = '<a$1href="$2.$3" rel=\'prettyPhoto\'$4>';
	$content = preg_replace( $pattern, $replacement, $content );
	return $content;
}
add_filter( 'the_content', 'insert_prettyPhoto_rel' );
add_filter( 'wp_get_attachment_link', 'gallery_prettyPhoto');


// function to display number of posts.
function tmnf_post_views($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count.'';
}

// function to count views.
function tmnf_count_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views',5,2);
function posts_column_views($defaults){
    $defaults['post_views'] = __('Views', 'themnific');
    return $defaults;
}
function posts_custom_column_views($column_name, $id){
	if($column_name === 'post_views'){
        echo tmnf_post_views(get_the_ID());
    }
}



// pagination
function tmnf_pagination($prev = '&laquo;', $next = '&raquo;') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    $tmnf_pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => $prev,
        'next_text' => $next,
        'type' => 'plain'
);
    if( $wp_rewrite->using_permalinks() )
        $tmnf_pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if( !empty($wp_query->query_vars['s']) )
        $pagination['add_args'] = str_replace(' ', '+', array( 's' => get_query_var( 's' ) ));

    echo paginate_links( $tmnf_pagination );
};


//Breadcrumbs
function tmnf_breadcrumbs() {
	if (!is_home()) {
		echo '<a href="'. home_url().'">';
		echo 'Inicio ';
		echo "</a> &raquo; ";
		if (is_category() || is_single()) {
		the_category(', ');
		if (is_single()) {
		echo " &raquo; ";
	echo short_title('...', 7);
	}
	} elseif (is_page()) {
	echo the_title();}
	}
}


function attachment_toolbox($size = thumbnail) {
	if($images = get_children(array(
		'post_parent'    => get_the_ID(),
		'post_type'      => 'attachment',
		'numberposts'    => -1, // show all
		'post_status'    => null,
		'post_mime_type' => 'image',
	))) {
		foreach($images as $image) {
			$attimg   = wp_get_attachment_image($image->ID,$size);
			$atturl   = wp_get_attachment_url($image->ID);
			$attlink  = get_attachment_link($image->ID);
			$postlink = get_permalink($image->post_parent);
			$atttitle = apply_filters('the_title',$image->post_title);

			echo '<p><strong>wp_get_attachment_image()</strong><br />'.$attimg.'</p>';
			echo '<p><strong>wp_get_attachment_url()</strong><br />'.$atturl.'</p>';
			
		}
	}
}

// allow contributor uploads
if ( current_user_can('contributor') && !current_user_can('upload_files') )
	add_action('admin_init', 'tmnf_allow_contributor_uploads');

function tmnf_allow_contributor_uploads() {
	$contributor = get_role('contributor');
	$contributor->add_cap('upload_files');
}

//Featured image in RSS feeds
function tmnf_image_in_rss($content)
{
    global $post;
    if (has_post_thumbnail($post->ID))
    {
        $content = get_the_post_thumbnail($post->ID, 'small', array('style' => 'margin-bottom:10px;')) . $content;
    }
    return $content;
}
add_filter('the_excerpt_rss', 'tmnf_image_in_rss');
add_filter('the_content_feed', 'tmnf_image_in_rss');


// meta sections
function tmnf_meta_small() {
	?>    
	<p class="meta">
		<?php the_time(get_option('date_format')); ?>  
        
    <?php
}

function tmnf_meta_small2() {
	?>    
	<p class="meta"> 
        
		<?php if(tmnf_ratings()){ ?> &bull; <?php echo tmnf_ratings(); } else { }?></p>
    <?php
}

function tmnf_meta() { ?>   
	<p class="meta">
		<?php the_time(get_option('date_format')); ?> &bull; 
		<?php the_category(', ') ?>  
        
    </p>
<?php }

function tmnf_meta_full() { ?>    
	<p class="meta">
		<?php the_time(get_option('date_format')); ?> &bull; 
		<?php the_category(', ') ?>  
              <?php comments_popup_link( __('Comments (0)', 'themnific'), __('Comments (1)', 'themnific'), __('Comments (%)', 'themnific')); ?>  &bull; 
        
    </p>
<?php
}

function tmnf_meta_more() {
	?>    
	<p class="meta_more">
    		<a href="<?php tmnf_link() ?>"><?php _e('Read article','themnific');?></a>
    </p>
    <?php
}

// get featured image on posts screen  
function tmnf_get_featured_image($post_ID) {  
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);  
    if ($post_thumbnail_id) {  
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');  
        return $post_thumbnail_img[0];  
    }  
} 
    // ADD NEW COLUMN  
    function tmnf_columns_head($defaults) {  
        $defaults['featured_image'] = 'Featured Image';  
        return $defaults;  
    }  
    // SHOW THE FEATURED IMAGE  
    function tmnf_columns_content($column_name, $post_ID) {  
        if ($column_name == 'featured_image') {  
            $post_featured_image = tmnf_get_featured_image($post_ID);  
            if ($post_featured_image) {  
                echo '<img style=" width:100px;" src="' . $post_featured_image . '" />';  
            }  
        }  
    }  
add_filter('manage_posts_columns', 'tmnf_columns_head');  
add_action('manage_posts_custom_column', 'tmnf_columns_content', 10, 2); 

// Authot image url ///

function modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['photo'] = 'Url de la foto';

	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');

//////////
//WooCommerce
//////////



// Override theme default specification for product # per row
function loop_columns() {
return 5; // 5 products per row
}
add_filter('loop_shop_columns', 'loop_columns', 999);


// woocommerce images

//Hook in on activation
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'tmnf_woocommerce_image_dimensions', 1 );
 
// Define image sizes
function tmnf_woocommerce_image_dimensions() {
$catalog = array(
'width' => '184',	// px
'height'	=> '184',	// px
'crop'	=> 1 // true
);
 
// Woo Image sizes
update_option( 'shop_catalog_image_size', $catalog ); // Product category thumbs
}


// limit related na upsells posts

	function woo_related_products_limit() {
	global $product;
	$related = '';
	$args = array(
	'post_type' => 'product',
	'no_found_rows' => 1,
	'posts_per_page' => 1,
	'ignore_sticky_posts' => 1,
	'post__in' => $related,
	'post__not_in' => array($product->id)
	);
	return $args;
	}
	add_filter( 'woocommerce_related_products_args', 'woo_related_products_limit' );
	
	
	
	// first remove the action defined in woocommerce-hooks.php
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	 
	// modeled off of the woocommerce_upsell_display() function in woocommerce-template.php
	// change $columns to desired number of columns
	function my_woocommerce_upsell_display( $posts_per_page = '-1', $columns = 1, $orderby = 'rand' ) {
	woocommerce_get_template( 'single-product/up-sells.php', array(
	'posts_per_page' => $posts_per_page,
	'orderby' => $orderby,
	'columns' => $columns
	) );
	}
	// re-add the action for display
	add_action( 'woocommerce_after_single_product_summary', 'my_woocommerce_upsell_display', 15 );

	//poner la primera imagen del post como imagen destacada
function wpforce_featured() {
          global $post;
          $already_has_thumb = has_post_thumbnail($post->ID);
              if (!$already_has_thumb)  {
              $attached_image = get_children( "post_parent=$post->ID&post_type=attachment&post_mime_type=image&numberposts=1" );
                          if ($attached_image) {
                                foreach ($attached_image as $attachment_id => $attachment) {
                                set_post_thumbnail($post->ID, $attachment_id);
                                }
                           }
                        }
      }  //end function
add_action('the_post', 'wpforce_featured');
add_action('save_post', 'wpforce_featured');
add_action('draft_to_publish', 'wpforce_featured');
add_action('new_to_publish', 'wpforce_featured');
add_action('pending_to_publish', 'wpforce_featured');
add_action('future_to_publish', 'wpforce_featured');

?>