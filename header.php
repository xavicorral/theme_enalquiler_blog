<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title><?php global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); $site_description = get_bloginfo( 'description', 'display' ); echo " | $site_description"; if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s','themnific'), max( $paged, $page ) ); ?></title>

<!-- Set the viewport width to device width for mobile -->

<?php if (get_option('themnific_res_mode_general') <> "true") { ?>
<style type="text/css">
        
    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Light-webfont...');
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Light-webfont...') format('emb
edded-opentype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.woff') format('woff'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.ttf') format('truetype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.svg#OpenSansLight') format('svg');
        font-weight: 300;
        font-style: normal
    }

    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.eot');
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.woff') format('woff'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.ttf') format('truetype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.svg#OpenSansRegular') format('svg');
        font-weight: 400;
        font-style: normal
    }

    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.eot');
        src: url('http://static.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.woff') format('woff'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.ttf') format('truetype'),url('http://static.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.svg#OpenSansBold') format('svg');
        font-weight: 600;
        font-style: normal
    }

   
</style>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<?php } ?>
<script type='text/javascript'>
googletag.cmd.push(function() {
        googletag.defineSlot('/42152709/social', [[980, 90],[728, 90]], 'div-gpt-ad-1373450611455-0').addService(googletag.pubads());
        googletag.pubads().collapseEmptyDivs();
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
});
</script>
<?php themnific_head(); ?>
<?php wp_head(); ?>
</head>

     
<body <?php if (get_option('themnific_upper') == 'false' ){ body_class( );} else body_class('upper' ) ?> itemscope itemtype="http://schema.org/WebPage">

<?php 
    $categories = get_the_category();
    $category_id = $categories[0]->cat_ID;
    if ( $category_id == 50985 ) {
        echo "<iframe id='ddx_ifr' src='' style='display:none'></iframe>
                <script>
                    ddx_vars='';
                    var doc = document.getElementById('ddx_ifr').document;
                    if(document.getElementById('ddx_ifr').contentDocument){doc = document.getElementById('ddx_ifr').contentDocument}
                    else if(document.getElementById('ddx_ifr').contentWindow){doc = document.getElementById('ddx_ifr').contentWindow.document;}
                    doc.open();
                    doc.writeln('<html><body><script src=\'http://www.delidatax.net/dmp/1132.php?'+ddx_vars+'\'><\/script><body></html>');
                    doc.close();
                </script>
            ";
    } else {
        echo "<iframe id='ddx_ifr' src='' style='display:none'></iframe>
                <script>
                    ddx_vars='';
                    var doc = document.getElementById('ddx_ifr').document;
                    if(document.getElementById('ddx_ifr').contentDocument){doc = document.getElementById('ddx_ifr').contentDocument}
                    else if(document.getElementById('ddx_ifr').contentWindow){doc = document.getElementById('ddx_ifr').contentWindow.document;}
                    doc.open();
                    doc.writeln('<html><body><script src=\'http://www.delidatax.net/dmp/1131.php?'+ddx_vars+'\'><\/script><body></html>');
                    doc.close();
                </script>
            ";
    }
?>

<header>
    <div class="container">
        <a title="Especialistas en pisos en alquiler" href="http://www.enalquiler.com/" class="logo-ena">
            <img src="data:image/gif;base64,R0lGODlh0gAyALMAAABqqe/2+mZmZjMzM0CPvszMzIC01GCiyR98s7/a6YyMjP///0xMTJ26yX+rxJ/H3yH5BAAHAP8ALAAAAADSADIAAAT/cMlJq704ax0cOMEmjmRpnmiqrqz1IEAMGG1t33iupwkh/4TEbkgsGo+SwOHHjBFCyKh0Sp0YYM0srcrtelUJLBPxyiKE37R6HfBprWIgdMNcn+r29DX7rLSzM3MYeHkThBKHhVNlTWcZYWYPGol5iZSKRn9vIntNfReXa5Y/mFEBBoCfI5pNBoKIpKWjMqVHjGNoJ5CNkhSharMxtUaAWyudwQC1BszMFb/DNZ6vCcwEcTEIBL0UrLS+sdEW0OIrTLkSS4CNrwu7wuDf5fHw8zhAFqjr7BZu9bDyLjxw4I8JgYMOqEBLcAAbAVc2AigQMKBiRQENMAjYmKQBRYsX/wtkwFdB3z4mxiYUfBauwruTyugBQEchQaKDOFkG7FZwU8lmEJMY8DGhAcijAwS0s1jgI1KLCgbJIJAPZpN+LRdcemA16zkMNr2K3Snh5bqEOmMI6RFLAUgBCiYysKi0wtOkG51WjDpuqoUEgAFfMCmD5kqZf7uOVQs2WdqY3bDt4wYwxoOeygrQfWV0r12QCkRW6FyxHckT6mRQXnDYUFbWZnAeTEZzQtidjkt6aoYNwWNACz7ytaC5NAWLDdpNKD4gY1qqKLj+SAkbd9bbU6nRbrwYcmXvEuKoSseEstXiDDS4bX68ogjhfZ3wQFmh9XfdPw5cwA7+6/7cru0knf8MysWhX4BmHPDAes5h0JkA7Q0gwnoQPjffdPV1l+EPyvH3WG1lAXgfBR74dUGJ8iE4VVDBVSQaBgFYFKEIzMUHAHQaANYMTvRRYJ9WWYnHnXWFDfkPkEQeWRB1th1iUG0ybhClBFNmUKOFFyghWTEbHpkMkyF2B6I7IiLp5Q9jeqgiiFViMJeEE7SZpZynTaCEYjFQ92MyqzUpppH9aSjTAUABldo//n3GQAGMNuooox/NKAKdJvqIZ55dBprkTIB+2ClioKqomExs3mWqpFK6h+UEA45xUDM9qiTomp/S+t+sI4raFakXmHpqnKqmCic9OEqAWZ/86YmrmZzeumn/n2Fu+thvu9qq6KPYOoqqBpSmKCqIyWY6La/OnikDmGQum4xs7LKL1nelDmuCnL0Gi2CxzPpW7gzihvodumqqCHCZ292BZr3ylkCvBd3e+Bi+fp7br4qPHZhYdwOrGySGBhdpwUcvKmxvBg3jaxCgykprqQz6XryTkPuOS/HK3paQKAUMorDwZwmbaTITHTJhsbHLHgoAtAET/Vq05jYNHmHN2nywBQ/qPDIGDbccHi5JVIMZAnMEEIfMErRqmUsoHpn2B4Kcgg3ZzLrUBLQjTe0HcifsvO13QSQQgtF4HgBYT3AHENtBW1ZgthOIA+dv3BVs2e41jFlbwXoDKKAc1dZXI2wj0IZfChPcC6xtlQVbngQ3JYufhM7N3bx5EVx5beTi3pz3vA4NUIt+1eMLeHM6fniuvjTg+7xuN3F6mRqy3sD2bDqmC7TOsiu9Z7HaJcKrbsH0LK9tPFkSZA+I8h5n4NFTDGykOe6ef59F2A8cIJsBD7R92Tp9/rK/QfgLBUPigAAFBa9Q4ztSNwjCh4c8gCaws0ceAhCYXCRNghjM4AYuqMEOejBd5PugCOfBwRGaMBolPKEKFZHCFbpQDS18oQy5EMMZ2jAKNbyhDomQQxdGAAA7" alt="Especialistas en pisos en alquiler">
        </a>

        <nav class="main-nav">
            <ul class="list-inline hidden-xs">
                <li class="first"><a href="http://www.enalquiler.com/comunidad-alquiler.html">La comunidad</a></li>
                <li><a href="http://www.enalquiler.com/favoritos" data-ena-favourites-menu='' rel="nofollow"><i class="fa fa-star"></i> <span>Favoritos</span></a></li>
                <li class="active"><a href="http://blog.enalquiler.com">Blog</a></li>
            </ul>
        </nav>

        <div class="access-publish">
            <a class="publish-free btn btn-sm btn-warning" href="http://www.enalquiler.com/anunciar-piso-gratis" rel="nofollow">
                <i class="fa fa-plus"></i> Publica gratis
            </a>
          
        </div>
    </div>




  <div class="clearfix"></div>
    
        <div id="navigation_wrap">
        
            <a id="triggernav" href="#"><?php _e('MENU','themnific');?></a>
            
            <nav id="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
            
                <?php 
                    get_template_part('/includes/uni-navigation'); 
                    get_template_part('/includes/uni-searchform' );
                ?>
            
            </nav>
    
        </div>
        
    </div>
</header>

<div class="clearfix"></div>

<div class="container <?php if (get_option('themnific_res_mode_general') == 'false' ); else echo 'generalresp_alt'; ?> <?php if (get_option('themnific_upper') == 'false' ); else echo 'upper'; ?>" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">