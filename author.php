<?php get_header(); ?>

<div id="core">   
            
	<div id="content" class="eightcol"> 
        
			<?php
            $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
            ?>
            
            <?php if (have_posts()) : ?>
            
            <h1 class="leading"><?php echo $curauth->nickname; ?></h1>
            <h2 class="leading"></h2>
    
            <div class="clearfix"></div>

			<div class="postauthor postauthor_alt">
                  
            <?php $photoauthor = get_the_author_meta('photo');
            echo '<img class="postauthor" src="'.$photoauthor.'" />'; ?>

                <p class="authordesc">
                    <?php _e('Website','themnific');?>: <a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a><br/>
                    <?php echo $curauth->user_description; ?>
                    <br /><a href="<?php echo the_author_meta('googleplus') ?>">Google+ </a> 
                </p>
                
            </div>  

          <ul class="medpost">

				<?php while (have_posts()) : the_post(); ?>
      
						<?php if(has_post_format('gallery'))  {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('video')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('audio')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('image')){
                            echo get_template_part( '/includes/post-types/image' );
                        }elseif(has_post_format('link')){
                            echo get_template_part( '/includes/post-types/link' );
                        }elseif(has_post_format('quote')){
                            echo get_template_part( '/includes/post-types/quote' );
                            } else {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }?>
         		
                <?php endwhile; ?>   <!-- end post -->
                    
     			</ul><!-- end latest posts section-->
      
              <div class="pagination"><?php tmnf_pagination('&laquo;', '&raquo;'); ?></div>
  
              <?php else : ?>
  
                  <h1>Sorry, no posts matched your criteria.</h1>
                  <?php get_search_form(); ?><br/>

			<?php endif; ?>

        </div><!-- end #core .eightcol-->

    <?php get_sidebar(); ?>  

</div><!-- #core -->

<div class="clearfix"></div>
    
<?php get_footer(); ?>