<?php
/*
Template Name: Homepage
*/
?>
<?php get_header(); ?>

    <div id="core">
        
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
        <?php the_content(); ?>
         
        <?php endwhile; endif; ?>
        
    </div>
    <div class="clearfix"></div>

        
<?php get_footer(); ?>