<?php get_header(); ?>

<div id="core">

	<div id="content" class="eightcol">
            
		<?php if (have_posts()) : ?>
            
            <h2 class="leading"><?php echo $s; ?><br/>
            <span><?php _e('Resultados de tu búsqueda','themnific');?></span></h2>
    
            <div class="clearfix"></div>

      		<ul class="medpost">
          
				<?php while (have_posts()) : the_post(); ?>
      
						<?php if(has_post_format('gallery'))  {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('video')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('audio')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('image')){
                            echo get_template_part( '/includes/post-types/image' );
                        }elseif(has_post_format('link')){
                            echo get_template_part( '/includes/post-types/link' );
                        }elseif(has_post_format('quote')){
                            echo get_template_part( '/includes/post-types/quote' );
                            } else {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }?>
         		
                <?php endwhile; ?>   <!-- end post -->
                    
     			</ul><!-- end latest posts section-->
      
              <div class="pagination"><?php tmnf_pagination('&laquo;', '&raquo;'); ?></div>
  
              <?php else : ?>
  
                  <h1>Lo siento, no tenemos posts que se ajusten a lo que has buscado.</h1>
                  <?php get_search_form(); ?><br/>

			<?php endif; ?>

        </div><!-- end #core .eightcol-->

    <?php get_sidebar(); ?>  

</div><!-- #core -->

<div class="clearfix"></div>
    
<?php get_footer(); ?>